from SparseArray import StringMatcher
import argparse
import os

parser = argparse.ArgumentParser()
parser.add_argument('query', type=str, help='String to query')

if __name__ == "__main__":

    strings = os.environ['STRINGS'].split(',')

    args = parser.parse_args()
    string_matcher = StringMatcher(strings)

    query = args.query.split(',')
    print(string_matcher.match(query))


