FROM python:3.7
MAINTAINER Omar AITICHOU
ADD main.py /
WORKDIR /home/silkroad/Bureau/my_new_docker_build/

RUN pip install -r requirements.txt
EXPOSE 80
CMD ["python", "./main.py"]
