#!/bin/python3

import math
import os
import random
import re
import sys


class StringMatcher:

    def __init__(
        self,
        strings):
        self.strings = strings

    def match(
        self,
        queries):
        res = [] # result list initialization
        for i in queries :  # we loop on queries
            res.append(self.strings.count(i)) # occurence count for each "queries" element in the string list.
        return {query: count for query, count in zip(queries, res)} # return a dicionnary with the querie(key) and the occurence(value)
