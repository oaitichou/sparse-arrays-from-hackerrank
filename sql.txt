
/* Première partie du test */

SELECT date, sum(prod_price * prod_qty) AS 'ventes'
FROM TRANSACTIONS
WHERE date BETWEEN '2019-01-01' AND '2019-12-31'
GROUP BY date 

/*Seconde partie du test*/


SELECT client_id, ventes_meuble, ventes_deco	

FROM  (SELECT client_id, sum(prod_price * prod_qty) as ventes_meuble
       FROM TRANSACTIONS as Ts INNER JOIN PRODUCT_NOMENCLATURE  AS Pd ON Ts.prod_id = Pd.product_id
       WHERE (date BETWEEN '2019-01-01' AND '2019-12-31') and product_type = 'MEUBLE'
       GROUP BY client_id) as table_meuble
	   
	   INNER JOIN
	  
	  (SELECT client_id, sum(prod_price * prod_qty) as ventes_deco 
	   FROM TRANSACTIONS as Ts INNER JOIN PRODUCT_NOMENCLATURE  AS Pd ON Ts.prod_id = Pd.product_id
	   WHERE (date BETWEEN '2019-01-01' AND '2019-12-31') and product_type = 'DECO'
	   GROUP BY client_id) as table_deco
		
	   ON table_meuble.client_id = table_deco.client_id