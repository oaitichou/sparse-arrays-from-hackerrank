# Sparse arrays from hackerrank
# Introduction

There is a collection of input strings and a collection of query strings. For each query string, we need to determine how many times it occurs in the list of input strings.

# Technologies

Python 3
Docker
Flask
MYSQL


## Arguments

In this project, the "strings" argument is extracted from the environment variable STRINGS; the "query" is passed as a parameter when calling main.py. In both cases, we separate item with a comma ",".

###Examples

Here follows simple examples to use the project:

export STRINGS="ab,abc,ab"
python -m main ab,abc,bc

or

STRINGS="ab,abc,ab" python -m main ab,abc,bc

